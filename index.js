console.log("Hello, world");

let firstName = "John";

console.log("First Name: " + firstName);

let lastName = "Smith";

console.log("Last Name: " + lastName);

let Age = 30;

console.log("Age: " + Age);

console.log("Hobbies:")

let Hobbies = ["Biking", "Mountain Climbing", "Swimming"];

console.log(Hobbies);

console.log("Work Address:")

let workAddress = {
	houseNumber: '32',
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}

console.log(workAddress);

function printUserInfo(firstName, lastName, Age){
	console.log(firstName + ' ' + lastName + ' ' + "is " + Age + " years of age.");
};

printUserInfo('John', 'Smith', 30);

console.log("This was printed inside of a function")

function printHobbies(){
	console.log(Hobbies);
};

function invokeFunction(printHobbies){
	printHobbies();
}

invokeFunction(printHobbies)

console.log("This was printed inside of a function")

function printAddress(){
	console.log(workAddress);
};

function invokeFunction(printAddress){
	printAddress();
}

invokeFunction(printAddress)

function retunFunction(isMarried){
	return "The value of isMarried is: " + isMarried
};

console.log(retunFunction('true'));

let status = retunFunction('true');
console.log(status);


